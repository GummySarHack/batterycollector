// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Classes/Components/BoxComponent.h"

#include "SpawnVolume.generated.h"

UCLASS()
class BATTERYCOLLECTOR_API ASpawnVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnVolume();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// the pickup to spwan
	UPROPERTY(EditAnywhere, Category = "Spawning")
	TSubclassOf<class APickUp> WhatToSpawn;

	//when to spawn
	FTimerHandle SpawnTimer;

	// minimum spaw delay
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
	float SpawnDelayRangeLow;
	
	// maximum spaw delay
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
	float SpawnDelayRangeMax;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//return the value to know where to spawn object
	FORCEINLINE UBoxComponent* GetWhereToSpawn() const { return WhereToSpawn; }

	//find a random point within the volume
	UFUNCTION(BlueprintPure, Category = "Spawning")
	FVector GetRandomPointsInVolume();


	//toggles spawn on and off
	UFUNCTION(BlueprintCallable, Category = "Spawning")
		void SetSpawningActive(bool bWhatToSpawn);

private:
	//Box component to specify where the battery should spawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning", meta = (AllowPrivateAccess = "true"))
		UBoxComponent* WhereToSpawn;

	//Handles the spwanin
	void SpawnPickUp();

	//current delay spawn -- not to be set in BP
	float SpawnDelay;
};
