// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUp.h"
#include "BatteryPickUp.generated.h"

/**
 * 
 */
UCLASS()
class BATTERYCOLLECTOR_API ABatteryPickUp : public APickUp
{
	GENERATED_BODY()
	
public:

	//Set default values for this actor"s properties
	ABatteryPickUp();
	//override was collected function -- use implementation because it is a blueprint native event
	void WasCollected_Implementation() override;

	//public way to access battery power
	float GetPower();

protected:

	//Set the amount of power given to the character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtocted = "true"))
		float BatteryPower;
};
