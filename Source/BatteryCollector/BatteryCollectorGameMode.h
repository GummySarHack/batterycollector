// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Engine.h"

#include "BatteryCollectorGameMode.generated.h"


//enum to store current state of gameplay

UENUM(BlueprintType)
enum class EBatteryPlayState : uint8
{
	EPlaying,
	EGameOver,
	EWon,
	WUknown
};

UCLASS(minimalapi)
class ABatteryCollectorGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABatteryCollectorGameMode();

	virtual void Tick(float DeltaTime)override;

	// returns power to win
	UFUNCTION(BlueprintPure, Category = "Power")
		float GetPowerToWin() const;

	virtual void BeginPlay() override;

	// To return current playing state
	UFUNCTION(BlueprintPure, Category = "Power")
		EBatteryPlayState GetCurrentState() const;

	//Set new current state
	void SetCurrentState(EBatteryPlayState NewState);

protected:

	//rate at which character loses power
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
		float DecayRate = 0.0f;

	//Power needed to win the game
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
		float PowerToWin = 0.0f;

	//widget class to use for our HUD screen
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
		TSubclassOf<class UUserWidget> HUDWidgetClass;

	// Instance of a HUD
	UPROPERTY()
		class UUserWidget* CurrentWidget = nullptr;

private:
	// Keep track of current game state
	EBatteryPlayState CurrentState;
	
	TArray<class ASpawnVolume*> SpawnVolumeActors;

	//handles fucntion call that rely upon changing the play state our game
	void HandleNewState(EBatteryPlayState NewState);
};
