// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUp.generated.h"

UCLASS()
class BATTERYCOLLECTOR_API APickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//true when pickup can be use and false when PikcUp is desactivated
	bool bIsActive;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;	
	
	//returns the mesh for the pickup
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return PickUpMesh; }
	
	//Doesn't usually change things 
	UFUNCTION(BlueprintPure, Category = "PickUp")
		bool IsActive();

	//BlueprintCallable -- can be executed in a blueprint or level blueprint
	UFUNCTION(BlueprintCallable, Category = "PickUp")
		void SetActive(bool NewPickUpState);
		
	// Function to call when pickup is collected
	UFUNCTION(BlueprintNativeEvent)
		void WasCollected();
		virtual void WasCollected_Implementation();
private:

	//Represents the pickup in the level
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PickUp", meta = (AllowPrivateAccess = true))
		class UStaticMeshComponent* PickUpMesh;


};
