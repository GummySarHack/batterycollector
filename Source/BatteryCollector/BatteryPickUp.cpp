// Fill out your copyright notice in the Description page of Project Settings.


#include "BatteryPickUp.h"

//Constructior
ABatteryPickUp::ABatteryPickUp()
{
	GetMesh()->SetSimulatePhysics(true);

	//base power of the battery
	BatteryPower = 150.f;
}

void ABatteryPickUp::WasCollected_Implementation()
{
	//Use pickup behaviour
	Super::WasCollected_Implementation();

	//destroy the battery
	Destroy();
}

float ABatteryPickUp::GetPower()
{
	return BatteryPower;
}